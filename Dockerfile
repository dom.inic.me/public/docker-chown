FROM alpine:3.4

RUN apk add --no-cache bash

ADD ./rootfs/ /

ENTRYPOINT ["/bin/bash", "/app/entrypoint.sh"]
CMD [""]
