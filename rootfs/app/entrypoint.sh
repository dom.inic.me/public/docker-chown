#!/bin/bash

set -e

UID=${UID?Need UID}
GID=${GID?Need GID}
OPTS="-"

if [ ! -z "$RECURSIVE" ]; then
  OPTS+="R"
fi

if [ ! -z "$DELAY" ]; then
  sleep "$DELAY"
fi

echo "chown $OPTS $UID:$GID /data"

if [ ! -z "$DRYRUN" ]; then
  exit 0
fi


chown "$OPTS" "$UID:$GID" /data
