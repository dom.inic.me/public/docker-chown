# docker-chown

## Example Usage

```
docker run --rm -it -e "RECURSIVE=true" -e "UID=1" -e "GID=1" -e "DRYRUN=true" -v /tmp/chowntest:/data docker-chown
```
